#!/bin/bash
#
# STARTUP SCRIPT FOR CONFIGURATION PANEL
#       OF SMART MIRROR DEVICE
#

# VENV DIRECTORY
#export WORKON_HOME=~/Envs
#source /usr/local/bin/virtualenvwrapper.sh

# ENV TYPE
export FLASK_CONFIG=development
#export FLASK_CONFIG=production

export FLASK_APP=run.py

# RUN APP

flask run
