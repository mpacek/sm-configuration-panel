from flask import flash, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user

from . import auth
from forms import LoginForm, RegistrationForm
from .. import db
from ..models import User

@auth.route('/register', methods=['GET', 'POST'])
def register():
    """
    Handle requestes to the /register root
    Add a user to the database
    """
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,
                    password=form.password.data)

        db.session.add(user)
        db.session.commit()
        flash('You have sucessfully registered!. You man now login')

        return redirect(url_for('auth.login'))

    # load template
    return render_template('auth/register.html', form=form, title='Register')

@auth.route('/login', methods=['GET', 'POST'])
def login():
    """
    Handle request to the /login route
    Log a user
    """
    form = LoginForm()
    if form.validate_on_submit():

        # check if user exists in database
        # and if password matches
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):

            # let the user login
            login_user(user)

            return redirect(url_for('home.dashboard'))

        # if details are incorrect
        else:
            flash('Invalid username or password')

    # load template
    return render_template('auth/login.html', form=form, title='Login')


@auth.route('/logout')
@login_required
def logout():
    """
    Handle requests to the /logout root
    Let the user logout
    """

    logout_user()
    flash('You have sucessfully been logged out.')

    # redirect after logging out
    return redirect(url_for('auth.login'))
