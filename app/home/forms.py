from flask_wtf import Form
from wtforms import StringField, IntegerField
from wtforms.validators import InputRequired


class FlickrForm(Form):
    api_key = StringField('API KEY', validators=[InputRequired()])
    image_limit = IntegerField('Image Limit', validators=[InputRequired()])


class WeatherForm(Form):
    api_key = StringField('API KEY', validators=[InputRequired()])
    location = StringField('Location', validators=[InputRequired()])
