from flask import render_template, redirect, url_for
from flask_login import login_required
import sqlite3
from .forms import FlickrForm, WeatherForm
import psutil
import platform
import subprocess

from . import home


@home.route('/')
def homepage():
    return render_template('home/index.html', title="Main view")


@home.route('/dashboard')
@login_required
def dashboard():
    return render_template('home/dashboard.html', title="Dashboard")


@home.route('/widgets', methods=["POST", "GET"])
@login_required
def widgets():
    flickr_form = FlickrForm()
    conn = sqlite3.connect('/var/Databases/73fcb940d49477bf777e196445fabfda.sqlite')
    conn_cursor = conn.cursor()

    conn_cursor.execute('''SELECT * FROM flickr_config''')
    flickr_data = conn_cursor.fetchone()

    flickr_form.api_key.data = flickr_data[0]
    flickr_form.image_limit.data = flickr_data[1]

    weather_form = WeatherForm()

    conn_cursor.execute('''SELECT * FROM weather_config''')
    weather_data = conn_cursor.fetchone()

    weather_form.api_key.data = weather_data[0]
    weather_form.location.data = weather_data[1]

    conn_cursor.close()
    conn.close()

    return render_template('home/widgets.html', title="Widgets Configuration", flickr_form=flickr_form,
                           weather_form=weather_form)


@home.route('/widgets/flickr', methods=["POST"])
@login_required
def widget_flickr():
    flickr_form = FlickrForm()

    conn = sqlite3.connect('/var/Databases/73fcb940d49477bf777e196445fabfda.sqlite')
    conn_cursor = conn.cursor()

    if flickr_form.validate_on_submit():
        conn_cursor.execute('''SELECT count(*) as total FROM flickr_config''')
        result = conn_cursor.fetchone()
        total = result[0]

        if total > 0:
            conn_cursor.execute('''UPDATE flickr_config SET api_key = ?, image_limit = ?''', (flickr_form.api_key.data,
                                                                                              flickr_form.image_limit.data))
        else:
            conn_cursor.execute('''INSERT INTO flickr_config (api_key, image_limit) VALUES(?,?)''', (flickr_form.api_key.data,
                                                                                                    flickr_form.image_limit.data))
    conn.commit()
    conn_cursor.close()
    conn.close()

    return redirect(url_for('home.widgets'))


@home.route('/widgets/weather', methods=["POST"])
@login_required
def widget_weather():
    weather_form = WeatherForm()

    conn = sqlite3.connect('/var/Databases/73fcb940d49477bf777e196445fabfda.sqlite')
    conn_cursor = conn.cursor()

    if weather_form.validate_on_submit():
        conn_cursor.execute('''SELECT count(*) as total FROM weather_config''')
        result = conn_cursor.fetchone()
        total = result[0]

        if total > 0:
            conn_cursor.execute('''UPDATE weather_config SET api_key = ?, location = ?''', (weather_form.api_key.data,
                                                                                              weather_form.location.data))
        else:
            conn_cursor.execute('''INSERT INTO weather_config (api_key, location) VALUES(?,?)''',
                                (weather_form.api_key.data,
                                 weather_form.location.data))
    conn.commit()
    conn_cursor.close()
    conn.close()

    return redirect(url_for('home.widgets'))

# Get network interfaces details
def get_inet():
    inetaddr = psutil.net_if_addrs()
    inet_det = []
    for k, v in inetaddr.iteritems():
        if v[0][0] == 2:
            inet_det.append([k, v[0][1], v[0][2], v[0][3]])

    return inet_det

def get_uptime():
    raw = subprocess.check_output('uptime').replace(',','')
    days = int(raw.split()[2])
    if 'min' in raw:
        hours = 0
        minutes = int(raw[4])
    else:
        hours, minutes = map(int,raw.split()[4].split(':'))

    return "%d days, %d:%d" % (days, hours, minutes)

@home.route('/system')
@login_required
def system():
    memt = psutil.virtual_memory().total
    mema = psutil.virtual_memory().active
    memi = psutil.virtual_memory().inactive
    cpu = psutil.cpu_percent()
    arch = platform.processor()
    hostname = platform.node()
    kernel = platform.release()
    inet = get_inet()
    uptime = get_uptime()

    # Preparing memory lists
    mpoint = [p.mountpoint for p in psutil.disk_partitions()]
    mpointperc = [psutil.disk_usage(z).percent for z in [k for k in mpoint]]
    mpointuse = zip(mpoint, [psutil.disk_usage(z).percent for z in [k for k in mpoint]], mpointperc)

    return render_template('home/system.html', title="System Stats", cpu=cpu, memt=memt, mema=mema, memi=memi, mpointuse=mpointuse, inet=inet, uptime=uptime, kernel=kernel, hostname=hostname, arch=arch)
